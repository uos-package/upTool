#include "desktopconfig.h"
#include "ui_desktopconfig.h"

#include <QDebug>
#include <QFileDialog>

DesktopConfig::DesktopConfig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DesktopConfig)
{
    ui->setupUi(this);
    initUi();
}

DesktopConfig::~DesktopConfig()
{
    delete ui;
}

void DesktopConfig::initUi()
{
    Global::g_categoryMap->insert("网络应用", "network");
    Global::g_categoryMap->insert("社交沟通", "Chat");
    Global::g_categoryMap->insert("音乐欣赏", "Audio");
    Global::g_categoryMap->insert("视频播放", "Video");
    Global::g_categoryMap->insert("图形图像", "Graphics");
    Global::g_categoryMap->insert("游戏娱乐", "Game");
    Global::g_categoryMap->insert("办公学习", "Office");
    Global::g_categoryMap->insert("阅读翻译", "Translation");
    Global::g_categoryMap->insert("编程开发", "Development");
    Global::g_categoryMap->insert("系统管理", "System");
    Global::g_categoryMap->insert("其他应用", "Utility");

    for (auto it = Global::g_categoryMap->begin(); it != Global::g_categoryMap->end(); it++)
    {
        ui->cb_category->addItem(it.key());
    }

}

void DesktopConfig::saveConfig()
{
    Global::g_packageInfo->m_desktopExec = ui->le_exec->text();
    Global::g_packageInfo->m_desktopIcon = ui->le_icon->text();
    Global::g_packageInfo->m_desktopCategory = Global::g_categoryMap->value(ui->cb_category->currentText());
    Global::g_packageInfo->m_desktopName = ui->le_name->text();
    Global::g_packageInfo->m_softDir = ui->le_softdir->text();
}

void DesktopConfig::loadConfig()
{
    ui->le_exec->setText(Global::g_packageInfo->m_desktopExec);
    ui->le_icon->setText(Global::g_packageInfo->m_desktopIcon);
    for (auto it = Global::g_categoryMap->begin(); it != Global::g_categoryMap->end(); it++)
    {
        if (it.value() == Global::g_packageInfo->m_desktopCategory)
        {
            ui->cb_category->setCurrentText(it.key());
        }
    }
    ui->le_name->setText(Global::g_packageInfo->m_desktopName);
    ui->le_softdir->setText(Global::g_packageInfo->m_softDir);
}


void DesktopConfig::on_pb_exec_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, "请选择可执行文件路径...", "~/Desktop/test");
    if(filePath.isEmpty())
    {
        return;
    }
    ui->le_exec->setText(filePath);
}

void DesktopConfig::on_pb_softDir_clicked()
{
    QString filePath = QFileDialog::getExistingDirectory(this, "请选择文件路径...", "~/Desktop/test");
    if(filePath.isEmpty())
    {
        return;
    }
    ui->le_softdir->setText(filePath);
}

void DesktopConfig::on_pb_icon_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, "请选择图标文件路径...", "~/Desktop/test");
    if(filePath.isEmpty())
    {
        return;
    }
    ui->le_icon->setText(filePath);
}
