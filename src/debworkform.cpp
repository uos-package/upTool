#include "debworkform.h"
#include "ui_debworkform.h"

DebWorkForm::DebWorkForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DebWorkForm)
{
    ui->setupUi(this);
}

DebWorkForm::~DebWorkForm()
{
    delete ui;
}

void DebWorkForm::setWorkProgress(QString str)
{
    str = QString("--------------------%1--------------------").arg(str);
    ui->te_progress->append(str);
}
