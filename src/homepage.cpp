#include "homepage.h"
#include "ui_homepage.h"

#include <QFileDialog>

HomePage::HomePage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HomePage)
{
    ui->setupUi(this);
}

HomePage::~HomePage()
{
    delete ui;
}

void HomePage::saveConfig()
{
    Global::g_packageInfo->m_workdir = ui->le_workdir->text();
    Global::g_packageInfo->m_softName = ui->le_softName->text();
    Global::g_packageInfo->m_mainterName = ui->le_mainterName->text();
    Global::g_packageInfo->m_mainterEmail = ui->le_mainterEmail->text();
}

void HomePage::loadConfig()
{
    ui->le_mainterName->setText(Global::g_packageInfo->m_mainterName);
    ui->le_mainterEmail->setText(Global::g_packageInfo->m_mainterEmail);
}

void HomePage::on_pb_workDir_clicked()
{
    QString filePath = QFileDialog::getExistingDirectory(this, "请选择文件路径...", "./");
    if(filePath.isEmpty())
    {
        return;
    }
    ui->le_workdir->setText(filePath);
}

void HomePage::on_le_softName_textChanged(const QString &arg1)
{
    QString dir = QString("%1/%2").arg(ui->le_workdir->text(), ui->le_softName->text());
    QString config = QString("%1/config.json").arg(dir);
    QDir workDir;
    if (workDir.exists(dir))
    {
        emit signalWorkDirExists(dir);
    }
}
