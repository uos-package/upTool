#include "uospackagetool.h"
#include "ui_uospackagetool.h"

#include <QDir>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QByteArray>

#include <DTitlebar>

#include "workthread.h"

UosPackageTool::UosPackageTool(QWidget *parent) :
    DMainWindow(parent),
    ui(new Ui::UosPackageTool)
{
    ui->setupUi(this);
    setWindowIcon(QIcon("://upTool.png"));
    this->titlebar()->setIcon(QIcon("://upTool.png"));
    this->titlebar()->setTitle("upTool");

    initUi();
}

UosPackageTool::~UosPackageTool()
{
    delete ui;
}

void UosPackageTool::initUi()
{
    m_homePage = new HomePage();
    m_controlConfig = new ControlConfig();
    m_desktopConfig = new DesktopConfig();
    m_debWorkForm = new DebWorkForm();

    ui->stackedWidget->addWidget(m_homePage);
    ui->stackedWidget->addWidget(m_controlConfig);
    ui->stackedWidget->addWidget(m_desktopConfig);
    ui->stackedWidget->addWidget(m_debWorkForm);

    ui->stackedWidget->setCurrentWidget(m_homePage);

    connect(m_homePage, SIGNAL(signalWorkDirExists(QString)), this, SLOT(slotWorkDirExists(QString)));
}

void UosPackageTool::slotWorkProgress(QString str)
{
    m_debWorkForm->setWorkProgress(str);
}

void UosPackageTool::slotWorkDirExists(QString softDir)
{
    QString configPath = QString("%1/config.json").arg(softDir);
    QFile configFile(configPath);
    if (!configFile.open(QIODevice::ReadWrite))
    {
        qDebug() << "File open error!";
        return;
    }

    QByteArray allData = configFile.readAll();
    configFile.close();

    QJsonParseError jsonError;
    QJsonDocument docu = QJsonDocument::fromJson(allData, &jsonError);  //转化为 JSON 文档
    if (!docu.isNull() && (jsonError.error == QJsonParseError::NoError))   //解析未发生错误
    {
        if (docu.isObject())        //Json文档为对象
        {
            QJsonObject object = docu.object();  //转化为对象
            Global::g_packageInfo->m_mainterName = object["mainterName"].toString();
            Global::g_packageInfo->m_mainterEmail = object["mainterEmail"].toString();
            Global::g_packageInfo->m_packageName = object["packageName"].toString();
            Global::g_packageInfo->m_softVersion = object["softVersion"].toString();
            Global::g_packageInfo->m_softArch = object["softArch"].toString();
            Global::g_packageInfo->m_softHomepage = object["softHomepage"].toString();
            Global::g_packageInfo->m_softDepends = object["softDepends"].toString();
            Global::g_packageInfo->m_softDescribe = object["softDescribe"].toString();
            Global::g_packageInfo->m_desktopExec = object["desktopExec"].toString();
            Global::g_packageInfo->m_desktopIcon = object["desktopIcon"].toString();
            Global::g_packageInfo->m_desktopCategory = object["desktopCategory"].toString();
            Global::g_packageInfo->m_desktopName = object["desktopName"].toString();
            Global::g_packageInfo->m_softDir = object["softDir"].toString();
        }
    }

    m_homePage->loadConfig();
    m_controlConfig->loadConfig();
    m_desktopConfig->loadConfig();

}


void UosPackageTool::on_pb_next_clicked()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()+1);
    if (ui->stackedWidget->currentIndex() == 3)
    {

        m_homePage->saveConfig();
        m_controlConfig->saveConfig();
        m_desktopConfig->saveConfig();

        WorkThread *workThread = new WorkThread;
        connect(workThread, SIGNAL(signalWorkProgress(QString)), this, SLOT(slotWorkProgress(QString)));
        workThread->start();
    }

}

void UosPackageTool::on_pb_previous_clicked()
{
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex()-1);
}

void UosPackageTool::on_stackedWidget_currentChanged(int arg1)
{
    if (arg1 == 0)
    {
        ui->pb_previous->setText("首页");
    }
    else {
        ui->pb_previous->setText("上一步");
    }

    if (arg1 == 3)
    {
        ui->pb_next->hide();
    }
    else {
        ui->pb_next->show();
        ui->pb_next->setText("下一步");
    }
}

void UosPackageTool::on_pb_homepage_clicked()
{
    ui->stackedWidget->setCurrentWidget(m_homePage);
}

void UosPackageTool::on_pb_control_clicked()
{
    ui->stackedWidget->setCurrentWidget(m_controlConfig);
}

void UosPackageTool::on_pb_desktop_clicked()
{
    ui->stackedWidget->setCurrentWidget(m_desktopConfig);
}

void UosPackageTool::on_pb_work_clicked()
{
    ui->stackedWidget->setCurrentWidget(m_debWorkForm);
}
